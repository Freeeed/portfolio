/*/////////////////////////////////////////////////////////////////////
/////////////////////////// ANIM BLOC4 ////////////////////////////////
/////////////////////////////////////////////////////////////////////*/

const anim1 = document.getElementById('box1')
anim1.addEventListener('mouseover', function() {
    document.querySelector('.topleft').classList = "topleft animate"
    document.querySelector('.topright').classList = "topright animate"
    document.querySelector('.botleft').classList = "botleft animate"
    document.querySelector('.botright').classList = "botright animate"
})

anim1.addEventListener('mouseleave', function() {
    document.querySelector('.topleft').classList = "topleft"
    document.querySelector('.topright').classList = "topright"
    document.querySelector('.botleft').classList = "botleft"
    document.querySelector('.botright').classList = "botright"
})

const anim2 = document.getElementById('box2')
anim2.addEventListener('mouseover', function() {
    document.querySelector('#box2 .topleft').classList = "topleft animate"
    document.querySelector('#box2 .topright').classList = "topright animate"
    document.querySelector('#box2 .botleft').classList = "botleft animate"
    document.querySelector('#box2 .botright').classList = "botright animate"
})

anim2.addEventListener('mouseleave', function() {
    document.querySelector('#box2 .topleft').classList = "topleft"
    document.querySelector('#box2 .topright').classList = "topright"
    document.querySelector('#box2 .botleft').classList = "botleft"
    document.querySelector('#box2 .botright').classList = "botright"
})

const anim3 = document.getElementById('box3')
anim3.addEventListener('mouseover', function() {
    document.querySelector('#box3 .topleft').classList = "topleft animate"
    document.querySelector('#box3 .topright').classList = "topright animate"
    document.querySelector('#box3 .botleft').classList = "botleft animate"
    document.querySelector('#box3 .botright').classList = "botright animate"
})

anim3.addEventListener('mouseleave', function() {
    document.querySelector('#box3 .topleft').classList = "topleft"
    document.querySelector('#box3 .topright').classList = "topright"
    document.querySelector('#box3 .botleft').classList = "botleft"
    document.querySelector('#box3 .botright').classList = "botright"
})

const anim4 = document.getElementById('box4')
anim4.addEventListener('mouseover', function() {
    document.querySelector('#box4 .topleft').classList = "topleft animate"
    document.querySelector('#box4 .topright').classList = "topright animate"
    document.querySelector('#box4 .botleft').classList = "botleft animate"
    document.querySelector('#box4 .botright').classList = "botright animate"
})

anim4.addEventListener('mouseleave', function() {
    document.querySelector('#box4 .topleft').classList = "topleft"
    document.querySelector('#box4 .topright').classList = "topright"
    document.querySelector('#box4 .botleft').classList = "botleft"
    document.querySelector('#box4 .botright').classList = "botright"
})

const anim5 = document.getElementById('box5')
anim5.addEventListener('mouseover', function() {
    document.querySelector('#box5 .topleft').classList = "topleft animate"
    document.querySelector('#box5 .topright').classList = "topright animate"
    document.querySelector('#box5 .botleft').classList = "botleft animate"
    document.querySelector('#box5 .botright').classList = "botright animate"
})

anim5.addEventListener('mouseleave', function() {
    document.querySelector('#box5 .topleft').classList = "topleft"
    document.querySelector('#box5 .topright').classList = "topright"
    document.querySelector('#box5 .botleft').classList = "botleft"
    document.querySelector('#box5 .botright').classList = "botright"
})

const anim6 = document.getElementById('box6')
anim6.addEventListener('mouseover', function() {
    document.querySelector('#box6 .topleft').classList = "topleft animate"
    document.querySelector('#box6 .topright').classList = "topright animate"
    document.querySelector('#box6 .botleft').classList = "botleft animate"
    document.querySelector('#box6 .botright').classList = "botright animate"
})

anim6.addEventListener('mouseleave', function() {
    document.querySelector('#box6 .topleft').classList = "topleft"
    document.querySelector('#box6 .topright').classList = "topright"
    document.querySelector('#box6 .botleft').classList = "botleft"
    document.querySelector('#box6 .botright').classList = "botright"
})

/*/////////////////////////////////////////////////////////////////////
////////////////////////// RESPONSIVE SIDENAV /////////////////////////
/////////////////////////////////////////////////////////////////////*/

const openNav = document.getElementById("sidenav")
openNav.addEventListener('click', function() {
    document.getElementById("mySidenav").style.width = "250px";
})

const closenav = document.querySelectorAll("#mySidenav a")
closenav.forEach(element => {
    element.addEventListener('click', function() {
        document.getElementById("mySidenav").style.width = "0";
    })

});